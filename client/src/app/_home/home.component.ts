import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({ 
    selector: 'app-home',
    templateUrl: 'home.component.html'
 })
export class HomeComponent implements OnInit, OnDestroy {
    userInfo: {
        firstName: String,
        username: String,
    }

    constructor() {

    }

    ngOnInit() {

    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks

    }

}