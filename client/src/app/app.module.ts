import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { AppComponent } from './app.component';
import { HomeComponent } from './_home/home.component'
import { DocumentListComponent } from './components/document-list/document-list.component';
import { DocumentComponent } from './components/document/document.component';

import { Routing } from './app.routing'

const config: SocketIoConfig = { url: 'http://localhost:6001', options: {} };

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    Routing,
    SocketIoModule.forRoot(config)
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    DocumentListComponent,
    DocumentComponent
  ],
  providers: [
    


  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
