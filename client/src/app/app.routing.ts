import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './_home/home.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomeComponent
    },
    { 
        path: '**', 
        redirectTo: '/home' 
    }
];

export const Routing = RouterModule.forRoot(appRoutes);