import {Component, OnInit, OnDestroy} from '@angular/core';
import {Observable, Subscription} from 'rxjs';

import {DocumentService} from 'src/app/services/document.service';

@Component({
    selector: 'app-document-list',
    templateUrl: './document-list.component.html',
    styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit, OnDestroy {
    documents: Observable<string[]>;
    tracking_guid: any;
    currentDoc: string;
    private _docSub: Subscription;
    private _guidSub: Subscription;

    constructor(private documentService: DocumentService) { }

    ngOnInit() {
        this.documentService.getTrackingGuid();
        this.documents = this.documentService.documents;
        this._docSub = this.documentService.currentDocument.subscribe(doc => this.currentDoc = doc.id);
        this._guidSub = this.documentService.tracking_guid.subscribe((str) => {
            console.log(str);
            return this.tracking_guid = str;
        });
        console.log('this.tracking_guid ', this.tracking_guid );

    }

    ngOnDestroy() {
        this._docSub.unsubscribe();
    }

    loadDoc(id: string) {
        this.documentService.getDocument(id);
    }

    newDoc() {
        this.documentService.newDocument();
    }

}
