import { Injectable } from '@angular/core';

import { Socket } from 'ngx-socket-io';

import { Document } from '../models/document';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  currentDocument = this.socket.fromEvent<Document>('document');

  documents = this.socket.fromEvent<string[]>('documents');

  tracking_guid = this.socket.fromEvent<string[]>('tracking_guid');

  constructor(private socket: Socket) { }

  getTrackingGuid() {

      console.log('get_tracking_guid');

      this.socket.emit('get_tracking_guid')
  }

  getDocument(id: string) {

    this.socket.emit('getDoc', id);

  }

  newDocument() {

    this.socket.emit('addDoc', { id: this.docId(), doc: '' });

    this.socket.emit('login', { id: this.docId(), doc: '' });

  }

  editDocument(document: Document) {

    this.socket.emit('editDoc', document);

    this.socket.emit('logout', document);

  }

  private docId() {

    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;

  }
}
