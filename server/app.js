require('dotenv').config()

// npm install @flavioespinoza/log_log --save
const _log = require('@flavioespinoza/log_log')._log
const log = require('@flavioespinoza/log_log').log

const _error = require('./_helpers/error')

const Chance = require('chance')
const chance = new Chance()

const app = require('express')()

const server = require('http').Server(app)

const PORT = 6001

const io = require('socket.io')(server)

let documents = {}

let tracking_guid = null

const session = require('express-session')({
	secret: process.env.SESSION_SECRET,
	resave: true,
	saveUninitialized: true,
	cookie: {secure: true},
})

const sharedsession = require('express-socket.io-session')

// global error handler
app.use(_error)

// session
app.use(session)

app.get('/home', function (req, res, next) {

	log.lightCyan('/home', req)

})

server.listen(PORT, () => { _log.pink(`Node app.js listening on port ${PORT}`) })

io.use(sharedsession(session))

io.on('connection', (socket) => {

	log.cyan('socket.id', socket.id)

	socket.on('get_tracking_guid', () => {

		_log.pink('get_tracking_guid')

		tracking_guid = chance.guid()

	    socket.emit('tracking_guid', {tracking_guid})

	})

	// Accept a login event with user's data
	socket.on('login', function (userdata) {

		_log.cyan('login')
		log.cyan(JSON.stringify(userdata, null, 2))

		socket.handshake.session.userdata = userdata

		socket.handshake.session.save()
	})

	socket.on('logout', function (userdata) {

		_log.warn(`logout: ${JSON.stringify(socket.handshake.session.userdata)}`)

		log.red({userdata})

		if (socket.handshake.session.userdata) {

			delete socket.handshake.session.userdata

			log.lightYellow(JSON.stringify(socket.handshake.session.userdata))

			socket.handshake.session.save()

		}

	})

	let previousId

	const safeJoin = currentId => {
		socket.leave(previousId)
		socket.join(currentId, () => _log.red(`Socket joined room ${currentId}`))
		previousId = currentId
	}

	socket.on('getDoc', docId => {
		safeJoin(docId)
		socket.emit('document', documents[docId])
	})

	socket.on('addDoc', doc => {
		documents[doc.id] = doc
		safeJoin(doc.id)
		io.emit('documents', Object.keys(documents))
		socket.emit('document', doc)
	})

	socket.on('editDoc', doc => {
		documents[doc.id] = doc
		socket.to(doc.id).emit('document', doc)
	})

	io.emit('documents', Object.keys(documents))

})